!function ($) {

  $(function() {

    var $window = $(window)
    var $flash = $('#flash')

    $(window).resize(sizing);

    $(document).ready(sizing);

    $(document).scroll(function () {
      if ($window.width() >= 979) {
        var offsetTop = $('#empresa').offset().top - getFixedHeight()

        if ($window.scrollTop() >= offsetTop) {
          $('#header').addClass('fixed')
          $('body').css('padding-top', getFixedHeight())
        } else {
          $('#header').removeClass('fixed')
          $('body').css('padding-top', 0)          
        }
      }     
    })

    $('a[href="#topo"]').on('click', function () {
      $('html, body').animate({
        scrollTop: 0
      }, 800)
    })

    $('#header .navbar .nav > li > a').on('click', function () {
      href = $(this).attr('href')
      $('html, body').animate({
        scrollTop: $(href).offset().top - getFixedHeight()
      }, 800)
    })

    $("a[href='#contato']").on('click', function () {
      $('#contato-form').removeClass('hide')
    })

    $('#contato-form .close').on('click', function () {
      $(this).parents('form').addClass('hide')
    })

    $('[data-video-id]').on('click', function(e) {
      $('[data-video-id]').removeClass('active')
      $(this).addClass('active')
      renderVideo($(this).attr('data-video-id'))
      e.preventDefault()
    })

    $('#contato-form').submit(function (e) {
      var $this = this

      $.ajax({
          type: 'POST'
        , url: $($this).attr('action')
        , dataType: 'html'
        , data: $($this).serialize()
        , beforeSend: function () { $($this).addClass('loading') }
        , complete: function () { $($this).removeClass('loading') }
      }).done(function (data) {
        if (data.length) {
          $flash.html('<i class="icon-remove icon-white"></i> ' + data).show()
        } else {
          $this.reset()
          $flash.html('<i class="icon-ok icon-white"></i> Mensagem enviada com sucesso. Obrigado pelo contato.').show()
          setTimeout(function () {
            $flash.html('').hide()
          }, 5000) 
        }
      })
      return false
    })

    function getFixedHeight () {
      return $('#header').outerHeight()
    } 

    function sizing () {
      if ($window.width() >= 768) {
        $('.row > .col2').css('min-height', 0)
        $('.row > .col2').css('min-height', function () {
          return $(this).parents('.row').outerHeight() + 'px'
        })
      } else {
        $('.row > .col2').css('min-height', 0)        
      }

      if ($window.width() >= 979) {

      } else {
          $('#header').removeClass('fixed')
          $('body').css('padding-top', 0)          
      } 

      $('#produtos .aplicacoes .player')
        .css('height', function () {
          return $(this).parents('.col2').outerHeight() * 0.7 + 'px'
        })
        .css('line-height', function () {
          return $(this).height() + 'px'
        })

      if ($('#produtos .aplicacoes .player').attr('data-video-id'))
        renderVideo($('#produtos .aplicacoes .player').attr('data-video-id'))
    }

    function renderVideo (id) {
      var id = id;
      var params = { allowScriptAccess: 'always' }
      var atts = { id: 'myytplayer' + id }
      var $player = $('.player')
      var width = $player.outerWidth()
      var height = $player.outerHeight()

      $player.attr('data-video-id', id)
      $player.html('<div id="video-' + id + '"></div>')
      swfobject.embedSWF('http://www.youtube.com/v/' + id + '?enablejsapi=1&playerapiid=ytplayer&autoplay=1', 'video-' + id, width, height, '8', null, null, params, atts)
    } 

  })

}(window.jQuery)